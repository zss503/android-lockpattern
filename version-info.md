# `7.0.0` _(October 28th, 2015)_


## Dependencies

- [`com.android.support:support-annotations:+`][#com.android.support:*]


## Changes

- Made dependency [`com.android.support:support-annotations`][#com.android.support:*] always latest version.
- Removed `haibison.android.lockpattern.utils.`~~`LoadingDialog`~~.


[#com.android.support:*]: https://developer.android.com/tools/support-library/index.html
